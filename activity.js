//S30 Activity
//Obj 2
db.fruits.aggregate([
 {$match: {onSale: true}},
 {$count: "fruitsOnSale"},
 {$out: "onSaleFruits"}
]);

//obj 3

db.fruits.aggregate([
{$match: {stock: {$gte: 20}}},
{$count: "enoughStock" },
{$out: "stockLTE20"}
]);

//Obj 4
db.fruits.aggregate([
 {$match: {onSale: true}},
 {$group: {_id:"$supplier_id", avgPrice: {$avg: "$price"}}},
 {$out: "averagePrice"}
]);

db.fruits.find()
//Obj 5
db.fruits.aggregate([
{$match: {onSale: true}},
{$group: {_id:"$supplier_id", max_price: {$max: "$price"}}},
 {$out: "maxPriceSupplier"}
]); 


//Obj 6
db.fruits.aggregate([
{$match: {onSale: true}},
{$group: {_id:"$supplier_id", min_price: {$min: "$price"}}},
{$out: "minPriceSupplier"}
]); 
